package moe.guo.statusbarheader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class TopCropImageView extends ImageView {

    private long mTs;
    private int mChangetime;

    public TopCropImageView(Context context, int changetime) {
        super(context);
        mChangetime = changetime;
        setScaleType(ScaleType.MATRIX);
        setClipToOutline(true);
    }

    public TopCropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setScaleType(ScaleType.MATRIX);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        updateBG();
        recomputeImgMatrix();
    }

    @Override
    public void onWindowFocusChanged(boolean hint) {
        if (hint) {
            updateBG();
            recomputeImgMatrix();
        }
        super.onWindowFocusChanged(hint);
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        recomputeImgMatrix();
        return super.setFrame(l, t, r, b);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        recomputeImgMatrix();
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void recomputeImgMatrix() {
        final Matrix matrix = getImageMatrix();

        float scale;
        final int viewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int viewHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        if (getDrawable() == null) {
            return;
        }
        final int drawableWidth = getDrawable().getIntrinsicWidth();
        final int drawableHeight = getDrawable().getIntrinsicHeight();

        if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
            scale = (float) viewHeight / (float) drawableHeight;
        } else {
            scale = (float) viewWidth / (float) drawableWidth;
        }

        if (drawableWidth * scale < viewWidth) {
            scale = (float) viewWidth / (float) drawableWidth;
        } else if (drawableHeight * scale < viewHeight) {
            scale = (float) viewHeight / (float) drawableHeight;
        }

        matrix.setScale(scale, scale);
        setImageMatrix(matrix);
    }

    private void setCustomBackground() {
        File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/Pictures/StatusBarHeader/");
        boolean success = true;
        if (!dir.exists()) {
            success = dir.mkdirs();
        }
        File BG_image;
        ArrayList<File> images = new ArrayList<>();
        if (success) {
            for (File childfile : dir.listFiles()) {
                if (childfile.isFile() && !childfile.getName().equals(".nomedia")) {
                    images.add(childfile);
                }
            }
            if (!images.isEmpty()) {
                BG_image = images.get(new Random().nextInt(images.size()));

                if (BG_image != null && BG_image.exists()) {
                    Bitmap img = BitmapFactory.decodeFile(BG_image.getAbsolutePath());
                    setImageBitmap(img);
                }
            }
        }
    }

    protected void updateBG() {
        if (isUpdate()) {
            setCustomBackground();
        }
    }

    private boolean isUpdate() {
        long s = mTs;
        mTs = System.currentTimeMillis() / 1000;
        return mTs - s > mChangetime;
    }
}