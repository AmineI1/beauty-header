package moe.guo.statusbarheader;

import android.app.AndroidAppHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;

public class Hook implements IXposedHookZygoteInit, IXposedHookInitPackageResources {

    private static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    @Override
    public void initZygote(IXposedHookZygoteInit.StartupParam startupParam) throws Throwable {

    }
    private FrameLayout getCustomView (Context context , int color, int changetime, int statusbar_height ){
            FrameLayout fl = new FrameLayout(context);
            LinearLayout ll = new LinearLayout(context);
            if (color != 0) {
                ll.setBackgroundColor(color);
                ll.setVisibility(View.VISIBLE);
            } else {
                ll.setVisibility(View.GONE);
            }
            TopCropImageView iv = new TopCropImageView(context, changetime);
            iv.setMinimumHeight(statusbar_height);
            ll.setMinimumHeight(statusbar_height);
            ll.setClipChildren(true);
            ll.setClipToPadding(true);
            fl.addView(iv);
            fl.addView(ll);
            fl.setClipChildren(true);
            fl.setClipToPadding(true);
            return fl;
    }
    @Override
    public void handleInitPackageResources(
            final XC_InitPackageResources.InitPackageResourcesParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals("com.android.systemui")) {
            return;
        }
        String panelLayout = null;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            panelLayout = "qs_panel";
        }
        if (lpparam.res.getIdentifier("quick_status_bar_expanded_header", "layout", "com.android.systemui") != 0) {
            lpparam.res.hookLayout("com.android.systemui", "layout", "quick_status_bar_expanded_header", new XC_LayoutInflated() {
                @Override
                public void handleLayoutInflated(final LayoutInflatedParam liparam) throws Throwable {
                    int shadow = 0;
                    try {
                        InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/shadow"));
                        BufferedReader br = new BufferedReader(reader);
                        shadow = Integer.parseInt(br.readLine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int changetime = 3600;
                    try {
                        InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/changetime"));
                        BufferedReader br = new BufferedReader(reader);
                        String line = br.readLine();
                        if (Integer.parseInt(line) != 0) {
                            changetime = Integer.parseInt(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Context context = AndroidAppHelper.currentApplication();
                    int statusbarheight = 0;
                    int color = 0;
                    if (shadow != 0) {
                        try {
                            color = Color.parseColor("#" + Integer.toHexString(shadow) + "000000");
                        } catch (java.lang.IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                    int groupId = liparam.res.getIdentifier("header", "id", "com.android.systemui");
                    if (groupId != 0) {
                        ViewGroup navbar = liparam.view.findViewById(groupId);
                        boolean isImageView = navbar.getChildAt(0).getClass().getName().equals(moe.guo.statusbarheader.TopCropImageView.class.getName());
                        if (!isImageView) {
                            int statusbar_height = (int) liparam.res.getDimension(liparam.res.getIdentifier("status_bar_header_height", "dimen", "com.android.systemui"));
                            FrameLayout fl = getCustomView(context,color,changetime,statusbar_height);
                            navbar.addView(fl, 0, new LayoutParams(-1, statusbar_height));
                            statusbarheight = statusbar_height;
                        }
                    }
                    File height = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/height");
                    try {
                        if (!height.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            height.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(height.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(Integer.toString(statusbarheight));
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    File width = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/width");
                    try {
                        if (!width.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            width.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(width.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        Resources resources = context.getResources();
                        DisplayMetrics dm = resources.getDisplayMetrics();
                        out.write(Integer.toString(dm.widthPixels));
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        if (lpparam.res.getIdentifier("status_bar_expanded_header", "layout", "com.android.systemui") != 0) {
            lpparam.res.hookLayout("com.android.systemui", "layout", "status_bar_expanded_header", new XC_LayoutInflated() {
                @Override
                public void handleLayoutInflated(final LayoutInflatedParam liparam) throws Throwable {
                    int shadow = 0;
                    try {
                        InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/shadow"));
                        BufferedReader br = new BufferedReader(reader);
                        shadow = Integer.parseInt(br.readLine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int changetime = 3600;
                    try {
                        InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/changetime"));
                        BufferedReader br = new BufferedReader(reader);
                        String line = br.readLine();
                        if (Integer.parseInt(line) != 0) {
                            changetime = Integer.parseInt(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Context context = AndroidAppHelper.currentApplication();
                    int statusbarheight = 0;
                    int color = 0;
                    if (shadow != 0) {
                        try {
                            color = Color.parseColor("#" + Integer.toHexString(shadow) + "000000");
                        } catch (java.lang.IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                    //flyme & android 6.x
                    int groupId = liparam.res.getIdentifier("header", "id", "com.android.systemui");
                    if (groupId != 0) {
                        ViewGroup navbar = liparam.view.findViewById(groupId);
                        boolean isImageView = navbar.getChildAt(0).getClass().getName().equals(moe.guo.statusbarheader.TopCropImageView.class.getName());
                        if (!isImageView) {
                            int statusbar_height = (int) liparam.res.getDimension(liparam.res.getIdentifier("status_bar_header_height", "dimen", "com.android.systemui"));
                            FrameLayout fl = getCustomView(context,color,changetime,statusbar_height);
                            navbar.addView(fl, 0, new LayoutParams(-1, statusbar_height));
                            statusbarheight = statusbar_height;
                        }
                    }
                    //coloros30
                    groupId = liparam.res.getIdentifier("expand_notification_head", "id", "com.android.systemui");
                    if (groupId != 0) {
                        ViewGroup navbar = liparam.view.findViewById(groupId);
                        boolean isImageView = navbar.getChildAt(0).getClass().getName().equals(moe.guo.statusbarheader.TopCropImageView.class.getName());
                        if (!isImageView) {
                            int statusbar_height = (int) liparam.res.getDimension(liparam.res.getIdentifier("status_bar_header_height", "dimen", "com.android.systemui"));
                            FrameLayout fl = getCustomView(context,color,changetime,statusbar_height);
                            navbar.addView(fl, 0, new LayoutParams(-1, statusbar_height));
                            statusbarheight = statusbar_height;
                        }
                    }
                    //coloros32
                    groupId = liparam.res.getIdentifier("clock_expanded", "id", "com.android.systemui");
                    if (groupId != 0) {
                        ViewGroup navbar = (ViewGroup) liparam.view.findViewById(groupId).getParent();
                        boolean isImageView = navbar.getChildAt(0).getClass().getName().equals(moe.guo.statusbarheader.TopCropImageView.class.getName());
                        if (!isImageView) {
                            int statusbar_height = (int) liparam.res.getDimension(liparam.res.getIdentifier("status_bar_header_height", "dimen", "com.android.systemui"));
                            FrameLayout fl = getCustomView(context,color,changetime,statusbar_height);
                            navbar.addView(fl, 0, new LayoutParams(-1, statusbar_height));
                            statusbarheight = statusbar_height;
                        }
                    }
                    //miui
                    groupId = liparam.res.getIdentifier("expanded_header", "id", "com.android.systemui");
                    if (groupId != 0) {
                        ViewGroup navbar = (ViewGroup) liparam.view.findViewById(groupId).getParent();
                        boolean isImageView = navbar.getChildAt(0).getClass().getName().equals(moe.guo.statusbarheader.TopCropImageView.class.getName());
                        if (!isImageView) {
                            int statusbar_height = (int) liparam.res.getDimension(liparam.res.getIdentifier("status_bar_header_height", "dimen", "com.android.systemui"));
                            FrameLayout fl = getCustomView(context,color,changetime,statusbar_height);
                            navbar.addView(fl, 0, new LayoutParams(-1, statusbar_height));
                            statusbarheight = statusbar_height;
                        }
                    }

                    File height = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/height");
                    try {
                        if (!height.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            height.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(height.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(Integer.toString(statusbarheight));
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    File width = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/width");
                    try {
                        if (!width.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            width.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(width.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        Resources resources = context.getResources();
                        DisplayMetrics dm = resources.getDisplayMetrics();
                        out.write(Integer.toString(dm.widthPixels));
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (panelLayout != null) {
            lpparam.res.hookLayout("com.android.systemui", "layout", panelLayout, new XC_LayoutInflated() {
                @Override
                public void handleLayoutInflated(final LayoutInflatedParam liparam) throws Throwable {
                    int id = liparam.res.getIdentifier("quick_settings_panel", "id", "com.android.systemui");
                    Context context = AndroidAppHelper.currentApplication();
                    if (id != 0) {
                        float bottomdp = 48.0f;
                        boolean modlayout = true;
                        try {
                            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/modlayout"));
                            BufferedReader br = new BufferedReader(reader);
                            String line = br.readLine();
                            if (!line.equals("")) {
                                modlayout = Boolean.parseBoolean(line);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/bottomdp"));
                            BufferedReader br = new BufferedReader(reader);
                            String line = br.readLine();
                            if (Float.parseFloat(line) != 0) {
                                bottomdp = Float.parseFloat(line);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (modlayout) {
                            LinearLayout ll = liparam.view.findViewById(id);
                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ll.getLayoutParams();
                            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                                params.setMargins(0, dip2px(context, 100.0f), 0, dip2px(context, 8.0f));
                            } else {
                                params.setMargins(0, dip2px(context, 130.0f), 0, dip2px(context, bottomdp));
                            }
                            ll.setLayoutParams(params);
                        }
                    }
                }
            });
        }
    }
}
