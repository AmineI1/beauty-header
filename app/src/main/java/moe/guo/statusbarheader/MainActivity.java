package moe.guo.statusbarheader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import eu.chainfire.libsuperuser.Shell;

public class MainActivity extends Activity {
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button_restart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new RestartSystemUI()).execute();
            }
        });
        SeekBar seekBar = findViewById(R.id.seekBar);
        final TextView tv = findViewById(R.id.textView_shadow);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint({"SetTextI18n"})
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                File file = getExternalFilesDir(null);
                if (file != null) {
                    File shadow = new File(file.getPath() + "/shadow");
                    try {
                        if (!shadow.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            shadow.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(shadow.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(Integer.toString(i));
                        out.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                tv.setText(Integer.toString(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        int shadow = 0;
        int height = 0;
        int width = 0;
        int changetime = 3600;
        float bottomdp = 48.0f;
        boolean modlayout = true;
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/shadow"));
            BufferedReader br = new BufferedReader(reader);
            shadow = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/width"));
            BufferedReader br = new BufferedReader(reader);
            width = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/height"));
            BufferedReader br = new BufferedReader(reader);
            height = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/bottomdp"));
            BufferedReader br = new BufferedReader(reader);
            bottomdp = Float.parseFloat(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/modlayout"));
            BufferedReader br = new BufferedReader(reader);
            modlayout = Boolean.parseBoolean(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory().getPath() + "/Android/data/moe.guo.statusbarheader/files/changetime"));
            BufferedReader br = new BufferedReader(reader);
            changetime = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        TextView tvd = findViewById(R.id.textView_description);
        tvd.setText(String.format(getString(R.string.xposeddescriptionInfo), height, width));

        seekBar.setProgress(shadow);
        tv.setText(Integer.toString(shadow));

        final Switch bottomswitch = findViewById(R.id.switch1);
        if (bottomdp != 5.0f) {
            bottomswitch.setChecked(false);
        } else {
            bottomswitch.setChecked(true);
        }
        bottomswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                float bottomdp;
                if (b) {
                    bottomdp = 16.0f;
                } else {
                    bottomdp = 48.0f;
                }
                File file = getExternalFilesDir(null);
                if (file != null) {
                    File shadow = new File(file.getPath() + "/bottomdp");
                    try {
                        if (!shadow.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            shadow.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(shadow.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(Float.toString(bottomdp));
                        out.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        final Switch modswitch = findViewById(R.id.switch2);
        if (modlayout) {
            modswitch.setChecked(true);
            bottomswitch.setEnabled(true);
        } else {
            modswitch.setChecked(false);
            bottomswitch.setEnabled(false);
        }
        modswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    bottomswitch.setEnabled(true);
                } else {
                    bottomswitch.setEnabled(false);
                }
                File file = getExternalFilesDir(null);
                if (file != null) {
                    File shadow = new File(file.getPath() + "/modlayout");
                    try {
                        if (!shadow.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            shadow.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(shadow.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(Boolean.toString(b));
                        out.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        if (changetime == 0) {
            changetime = 3600;
        }
        final EditText editText = findViewById(R.id.editText);
        editText.setText(Integer.toString(changetime));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                File file = getExternalFilesDir(null);
                if (file != null) {
                    File shadow = new File(file.getPath() + "/changetime");
                    try {
                        if (!shadow.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            shadow.createNewFile();
                        }
                        FileWriter fstream = new FileWriter(shadow.getPath(), false);
                        BufferedWriter out = new BufferedWriter(fstream);
                        out.write(editText.getText().toString());
                        out.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static class RestartSystemUI extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            boolean suAvailable = Shell.SU.available();
            if (suAvailable) {
                Shell.SU.run(new String[]{"pkill -l 9 -f com.android.systemui", "killall com.android.systemui"});
            }
            return null;
        }
    }


}

